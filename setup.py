# -*- coding: utf-8 -*-

from setuptools import setup

setup(
        name="oi",
        version="0.1",
        packages=["oi"],
        package_dir={ "oi": "src/oi", },
        #url,
        licence="GPLv3+",
        author="MonkeysAreEvil",
        author_email="monkey@etage.io",
        description="Watch a process and fire a notification on completion",
        classifiers=[
            "Development Status :: 3 - Alpha",
            "Environment :: Console",
            "Intended Audience :: End Users/Desktop",
            "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
            "Operating System :: POSIX :: Linux",
            "Programming Language :: Python :: 3.6"
            "Topic :: Utilities",
            ],
        install_requires=["toml", "psutil"],
        entry_points={
            "console_scripts": [
                "oi = oi.__main__:main"
                ]
            },
        )
