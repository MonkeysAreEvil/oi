# oi

Watch a process and fire a notification on completion.

A nicer version of variadico/noti. Oi sends you messages on your computer or phone when a long running process finishes. Unlike noti, does not depend on external servies.

oi can send a notification to an Android phone, local desktop, or remote desktop (over ssh).

## Dependencies

For building oi (should be downloaded automatically)

```shell
toml
psutil
```

For showing messages on desktop:

```shell
notify-send
```

For showing messages on Android:

```shell
kde-connect
```

## Installation

Just clone the repo and run

```shell
python3 setup.py install --user
```

And then copy the config file to `~/.config/oi/oi.toml`.

## Examples

Just chuck oi at the start or end of commands

```shell
oi tar -cjf backup.tar.gz backup/
gcc -Wall -Werror foo.c -o foo; oi
```

If you started a process and forgot to attach oi, you can attach it after

```shell
oi --pid $(pgrep foo)
```


## Configuration

oi can be configured using a toml config file in <location>. Options are

```toml
# Specifies where to send notifications
[backends]
desktop = true # Notify to local desktop
android = true # Notify to Android phone. Requires kdeconnect to be installed and configured on PC and phone
# Notify to some remote PCs. Notifications are sent over SSH; host should be some valid string
# i.e. user@host or value in .ssh/config
# The window-manager var is used to connect to dbus and hence to send notifications
remote = [{host = "user@localhost", window-manager = "awesome"}]

# Specifies two useful flags on notify-send
[notify-send]
urgency = "normal"
expire-time = 1000

# Specifies the name of the paired Android phone
[kdeconnect]
name = "google"
```

Some of these can be overriden at runtime

```shell
--urgency, -u
--expire-time, -t
# Specifies a single backend to use
--backend, -b
```
