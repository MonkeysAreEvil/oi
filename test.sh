#!/bin/bash

# Generates a random exit_code from {-1,0,1}
rc=$(( ( RANDOM % 3 ) - 1 ))

# Current pid
echo $$

sleep 3

$(exit $rc)
