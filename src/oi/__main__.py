# pylint: disable=C0111
#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import sys
import argparse
from subprocess import run, check_output
from time import sleep
import psutil
import toml

def notify(name, exit_code, config):
    backends = config["backends"]
    urgency = config["notify-send"]["urgency"]
    expire_time = config["notify-send"]["expire-time"]
    android_name = config["kdeconnect"]["name"]

    message = ""
    if exit_code == 0 or exit_code is None:
        message = "Done!"
    else:
        message = "Finished with errors"

    if backends["desktop"]:
        cmd = [
            "notify-send",
            "--urgency", urgency,
            "--expire-time", str(expire_time),
            name,
            message
            ]
        run(cmd)
    if backends["android"]:
        cmd = [
            "kdeconnect-cli",
            "--name", android_name,
            "--ping-msg", name + ": " + message
            ]
        run(cmd)
    if backends["remote"]:
        for remote in backends["remote"]:
            window_manager = remote["window-manager"]
            host = remote["host"]
            # Need this env var to use notify-send over ssh
            # Note that the dbus bus address is per user/per session etc
            # So a simple echo of $DBUS_SESSION_BUS_ADDRESS is insufficient
            # Instead, parse the env vars of the window manager
            ssh = "cat /proc/$(pgrep %s)/environ | tr \"\\0\" \"\\n\" | grep DBUS_SESSION_BUS_ADDRESS | cut -d \"=\" -f2-" % (window_manager)
            bus_address = check_output(["ssh", host, ssh]).decode("utf-8")
            bus_address = bus_address.strip()
            ssh = "sh -c 'DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=%s notify-send --urgency %s --expire-time %i \"%s\" \"%s\"'" % (bus_address, urgency, expire_time, name, message)
            cmd = [
                "ssh",
                host,
                ssh
                ]
            run(cmd)

    sys.exit()

def watch_pid(pid, config):
    try:
        process = psutil.Process(pid)
    except:
        print("No process with specified pid found")
        sys.exit()
    name = process.name()
    while True:
        try:
            process.status()
        except:
            # I can't think of an elegnt way to retrieve the exit code
            notify(name, None, config)
        else:
            sleep(1)

def main():
    parser = argparse.ArgumentParser(description="Watch a process and fire a notification on completion.")
    parser.add_argument("--version", action="version", version="'%(prog)s 0.1'")
    parser.add_argument("--pid", type=int, help="Watches a process with the given pid")
    parser.add_argument("--urgency", "-u", help="Specifies the urgency level (low, normal, critical)")
    parser.add_argument("--expire-time", "-t", type=int, help="Specifies the timeout in milliseconds at which to expire the notification.")
    parser.add_argument("--backend", "-b", help="Specify the backend to use (desktop,android,remote).")

    args, remaining = parser.parse_known_args()
    args = vars(args)

    # Read the config file and determine how to notify
    home = os.getenv("HOME")
    config_file = open(home + "/.config/oi/oi.toml", "r")
    config = toml.loads(config_file.read())

    # cmd args override config file
    if args["backend"]:
        config["backends"] = [args["backends"]]
    if args["urgency"]:
        config["notify-send"]["urgency"] = args["urgency"]
    if args["expire_time"]:
        config["notify-send"]["expire_time"] = args["expire_time"]

    # Either watch a specific process
    if args["pid"]:
        watch_pid(args["pid"], config)
    # Or run the specified command
    elif remaining:
        rc = run(remaining)
        notify(remaining[0], rc.returncode, config)
    # Or run after the last command has finished
    else:
        # I can't think of an elegant way to retrieve the last command name or exit code
        notify(None, None, config)

if __name__ == '__main__':
    main()
